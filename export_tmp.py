from skimage import data, io, filters, draw, transform, color
import numpy as np
from pathlib import Path

def export(img):
    return (255 * color.lab2rgb(img)).astype('uint8')

def fix_folder():
    root = Path('.') 

    
    for x in root.glob('**/*.npy'):
        print(x)
        r2 = x.with_suffix(".png")
        io.imsave(str(r2),export(np.load(str(x),allow_pickle=True)))
        
        
    
fix_folder()