import numpy as np
from pathlib import Path
from sklearn.cluster import KMeans
from sklearn.feature_extraction.image import grid_to_graph
from sklearn.cluster import AgglomerativeClustering
from tqdm import tqdm
from collections import Counter

def calculate_clustering(original,ch):
    print('calculate_clustering')
    arr = original.reshape((-1, 3))
    connectivity = grid_to_graph(original.shape[0], original.shape[1])

    #model = KMeans(n_clusters=4, random_state=42).fit(arr)
    model = AgglomerativeClustering(n_clusters=100, linkage='ward',
                                   connectivity=connectivity, compute_full_tree=True, memory=ch)
    model.fit(arr)
    return model

def reduce_colors(original, model, n_colors=40):
    #print('reduce')
    arr = original.reshape((-1, 3))
    model.set_params(n_clusters=n_colors)
    model.fit(arr)
    labels = model.labels_
    #print(set(model.labels_))
    centers = []
    for x in range(n_colors):
        centers.append(arr[labels == x].sum(axis=0) / np.sum(labels == x))
    #print(labels)
    #print(centers)
    centers = np.array(centers)
    return labels,centers 

    
def build_with_colors(labels,colors,shape):
    return colors[labels].reshape(shape)

def process():
    mx=60
    for x in tqdm(list(Path('./storage').glob('*')),desc='scanning'):
        path_f = x / 'slides'
        print(path_f)
        if (path_f / 'finish.lock').exists():
            continue
        path_f.mkdir(exist_ok = True)
        path_base = (x / 'base').with_suffix('.npy')
        stem = path_base.parent.stem
        image = np.load(path_base)
        model = calculate_clustering(image, 'storage/' + stem + '/model')
        w,h,c = image.shape
        
        stack = {}
        for x in tqdm(range(1,mx+1),desc='reducing'):
            stack[x] = reduce_colors(image,model,x)
            
        
        loop = {}
        
        for k in tqdm(stack,desc='linking'):
            if k+1 not in stack:
                continue
            loop[k] = {}
            old = stack[k][0]
            new = stack[k+1][0]
            newset = set(new)
            counter = Counter(list(new))
            for x in set(old):
                counter = Counter(list(new[old == x]))
                #print(counter)
                loop[k][x] = max(newset, key = lambda x:counter.get(x,0))
        
        #print(loop)
        
        for k in tqdm(sorted(loop,key=lambda x:-x),desc='colors'):
            labels, colors = stack[k]
            _,colors_prev = stack[k+1]
            #print(colors.shape,colors_prev.shape)
            #print(colors)
            for x in range(k):
                colors[x,:] = colors_prev[loop[k][x],:]
            #print(colors)
            stack[k] = labels, colors
            
        imgs = []
        
        for k,v in tqdm(stack.items(),desc='building images'):
            img = build_with_colors(v[0],v[1],(w,h,c))
            imgs.append(img)
            
        final = stack[max(stack)][0]
        imgs_final = imgs[-1]
        w,h,c = imgs_final.shape
        for k in tqdm(reversed(range(max(stack)+2)),desc='building final',total=max(stack)+2):
            filter_ = (final < k).reshape(w,h,1)
            img = imgs_final*filter_ + image*~filter_
            imgs.append(img)
        
        for i,x in enumerate(imgs):
            np.save((path_f /  f"{i:04}").with_suffix('.npy'),x)
        (path_f / 'finish.lock').touch()
            


if __name__ == "__main__":            
    process()
        
    
    
    
    
    
    

    
    
    


