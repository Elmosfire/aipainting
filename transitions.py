import numpy as np
from pathlib import Path
from skimage import data, io, filters, draw, transform, color
from tqdm import tqdm
from numba import jit
from multiprocessing import Pool


from video import save

@jit(nopython=True,parallel=False)
def fast_mult(prev1,current1,filter1,keyframe):
    r = (prev1 * filter1) + (current1* (1-filter1))
    return r, np.sum(np.abs(r-keyframe)) > 100

    
def slow_paint(prev,current,transfer):
    #assert len(transfer.shape) == 2,f"transfer has shape {transfer.shape}"
    keyframe = prev
    yield keyframe
    for filter_ in transfer:
        filteru = np.broadcast_to(filter_[:,:,None],prev.shape)
        res,t = fast_mult(prev,current,filteru,keyframe)
        if t:
            keyframe = res
            yield keyframe
            
def transfer():
    for x in list(sorted(list(Path('./transition').glob('*.npy')))):
        yield np.load(x).T
        

def create_video_transition(start,end_):
    prev = np.load(start)
    current = np.load(end_)
    #print(prev.shape,current.shape)
    return slow_paint(prev,current,transfer())
    
def create_video_transition_batch(frames):

    frames = list(frames)
    clips = []
    for x,y in zip(frames[:-1],frames[1:]):
        yield from create_video_transition(x,y)
        
def create_video_transition_wrapper(args):
    fname,frames = args
    save(create_video_transition_batch(frames),fname)  
        
    

    
def get_all(x,batchsize):
    if not (x /'slides' / 'finish.lock').exists():
        return 
    if (x /'fluid' / 'finish.lock').exists():
        return 
    (x / 'fluid').mkdir(exist_ok=True)
    for y in tqdm(list(sorted(x.glob('slides/*.npy'))),desc='fluid'):
        i = int(y.stem)
        if not i%batchsize==0:
            continue
        
        frames = [str(x / 'slides' / f"{i+j:04}.npy") for j in range(batchsize+1) if (x / 'slides' / f"{i+j:04}.npy").exists()]
        
        if len(frames) <= 1:
            continue
        vid = x / 'fluid' / f"{i:04}.mp4"
        if vid.exists():
            continue
        #print(frames)
        yield (str(vid),tuple(frames))
        
def transitions():
    for x in tqdm(list(Path('./storage').glob('*/')),desc='scanning'):
        with Pool(5) as p:
            print(p.map(create_video_transition_wrapper, get_all(x,10)))
        (x /'fluid' / 'finish.lock').touch()
        
if __name__ == '__main__':
    transitions()