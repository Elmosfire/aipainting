import asyncio
from pathlib import Path
from tqdm import tqdm

async def run(cmd):
    proc = await asyncio.create_subprocess_shell(
        cmd,
        stdout=asyncio.subprocess.PIPE,
        stderr=asyncio.subprocess.PIPE)

    stdout, stderr = await proc.communicate()

    print(f'[{cmd!r} exited with {proc.returncode}]')
    if stdout:
        print(f'[stdout]\n{stdout.decode()}')
    if stderr:
        print(f'[stderr]\n{stderr.decode()}')
        
def get_all(x,batchsize=1):
    if not (x /'slides' / 'finish.lock').exists():
        return 
    if (x /'fluid' / 'finish.lock').exists():
        return 
    (x / 'fluid').mkdir(exist_ok=True)
    for y in tqdm(list(sorted(x.glob('slides/*.npy'))),desc='fluid'):
        i = int(y.stem)
        if not i%batchsize==0:
            continue
        
        frames = [str(x / 'slides' / f"{i+j:04}.npy") for j in range(batchsize+1) if (x / 'slides' / f"{i+j:04}.npy").exists()]
        
        if len(frames) <= 1:
            continue
        vid = x / 'fluid' / f"{i:04}.mp4"
        if vid.exists():
            continue
        #print(frames)
        yield (str(vid),tuple(frames))
        
def buildcommand(key):
    a,(b,c) = key
    return f'bash transition.sh {b} {c} {a}'

async def main():
    for x in tqdm(list(Path('./storage').glob('*/')),desc='scanning'):
        commands = map(buildcommand,get_all(x))
        runs = [run(x) for x in commands]
        print(runs)
        await asyncio.gather(*runs)

asyncio.run(main())
