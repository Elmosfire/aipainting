from pathlib import Path
from hashlib import sha256
from tqdm import tqdm
from skimage import data, io, filters, draw, transform, color
import numpy as np
import base64

def scan():
    yield from Path('./selected').glob('**/*.jpg')
    yield from Path('./selected').glob('**/*.png')

def preproc():
    for x in tqdm(list(scan())):
        print(x.absolute())
        
        name = base64.urlsafe_b64encode(sha256(x.stem.encode()).digest()).decode()
        exname = (Path('./storage') / name / 'base').with_suffix('.npy')
        #exname2 = (Path('./cleaned_debug') / name).with_suffix('.png')
        exname.parent.mkdir(exist_ok=True,parents=True)
        if exname.exists():
            continue
        i = io.imread(str(x))
        try:
            j = preprocess_landscape(i,640,480,0)
        except IndexError:
            continue
        with exname.open('wb') as file:
            np.save(file, j)
        #io.imsave(exname2,export(j))
        
        
        
def preprocess_landscape(array, w, h, brushsize):
    print('processing landscape')
    if len(array.shape) == 2:
        arr2 = np.zeros((array.shape[0], array.shape[1], 3))
        arr2[:, :, 3] = array[:, :, np.newaxis]
    else:
        arr2 = array[:, :, :3]
    scale = max(w / array.shape[1], h / array.shape[0])
    arr3 = transform.rescale(arr2, scale, mode="reflect", multichannel=True)
    arr4 = np.pad(arr3, ((brushsize, brushsize), (brushsize, brushsize), (0, 0)), constant_values=255)
    rh, rw, _ = arr4.shape
    x = (rw - w + brushsize * 2) // 2
    y = (rh - h + brushsize * 2) // 2
    arr5 = arr4[y:y + h - brushsize * 2, x:x + w - brushsize * 2, :]
    arr6 = np.pad(arr5, ((brushsize, brushsize), (brushsize, brushsize), (0, 0)), constant_values=255)
    print(array.shape,arr2.shape,arr3.shape,arr4.shape,arr5.shape,arr6.shape)
    return color.rgb2lab(arr6)


if __name__ == '__main__':
    preproc()