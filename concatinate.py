from pathlib import Path
from video import concat

for x in Path('./storage').glob('*/'):
    if not (x /'slides' / 'finish.lock').exists():
        continue 
    if not (x /'fluid' / 'finish.lock').exists():
        continue 
    l = sorted(list((x / 'fluid').glob('*.mp4')))
    print(l)
    if l:
        concat(l,str(x/'final.mp4'))