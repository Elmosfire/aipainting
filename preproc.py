"""
This file generates the different movies required to stream.
"""
import numpy as np
from sklearn.cluster import KMeans
from sklearn.feature_extraction.image import grid_to_graph
from sklearn.cluster import AgglomerativeClustering
from skimage import data, io, filters, draw, transform, color
import scipy.signal
from pathlib import Path
from hashlib import sha256
import base64
from tqdm import tqdm
from collections import Counter
from random import choice
from itertools import chain
from moviepy.editor import VideoClip, concatenate_videoclips, VideoFileClip
import argparse
from functools import lru_cache
from numba import jit
import pickle


def preprocess_landscape(array, w, h, brushsize):
    print('processing landscape')
    if len(array.shape) == 2:
        arr2 = np.zeros((array.shape[0], array.shape[1], 3))
        arr2[:, :, 3] = array[:, :, np.newaxis]
    else:
        arr2 = array[:, :, :3]
    scale = max(w / array.shape[1], h / array.shape[0])
    arr3 = transform.rescale(arr2, scale, mode="reflect", multichannel=True)
    arr4 = np.pad(arr3, ((brushsize, brushsize), (brushsize, brushsize), (0, 0)), constant_values=255)
    rh, rw, _ = arr4.shape
    x = (rw - w + brushsize * 2) // 2
    y = (rh - h + brushsize * 2) // 2
    arr5 = arr4[y:y + h - brushsize * 2, x:x + w - brushsize * 2, :]
    arr6 = np.pad(arr5, ((brushsize, brushsize), (brushsize, brushsize), (0, 0)), constant_values=255)
    print(array.shape,arr2.shape,arr3.shape,arr4.shape,arr5.shape,arr6.shape)
    return color.rgb2lab(arr6)

def calculate_clustering(original,ch):
    print('calculate_clustering')
    arr = original.reshape((-1, 3))
    connectivity = grid_to_graph(original.shape[0], original.shape[1])

    #model = KMeans(n_clusters=4, random_state=42).fit(arr)
    model = AgglomerativeClustering(n_clusters=100, linkage='ward',
                                   connectivity=connectivity, compute_full_tree=True, memory=ch)
    model.fit(arr)
    return model

def reduce_colors(original, model, n_colors=40):
    #print('reduce')
    arr = original.reshape((-1, 3))
    model.set_params(n_clusters=n_colors)
    model.fit(arr)
    labels = model.labels_
    #print(set(model.labels_))
    centers = []
    for x in range(n_colors):
        centers.append(arr[labels == x].sum(axis=0) / np.sum(labels == x))
    #print(labels)
    #print(centers)
    centers = np.array(centers)
    return labels,centers
    
def build_with_colors(labels,colors,shape):
    return colors[labels].reshape(shape)

def export(img):
    return (255 * color.lab2rgb(img)).astype('uint8')


def scan():
    yield from Path('./Landscape').glob('**/*.jpg')
    yield from Path('./Landscape').glob('**/*.png')
    
def scanv():
    yield from Path('./Cleaned').glob('*.png')

def preproc():
    for x in tqdm(list(scan())):
        print(x.absolute())
        
        name = base64.urlsafe_b64encode(sha256(x.stem.encode()).digest()).decode()
        exname = (Path('./Cleaned') / name).with_suffix('.png')
        if exname.exists():
            continue
        i = io.imread(str(x))
        try:
            j = preprocess_landscape(i,640,480,0)
        except IndexError:
            continue
        io.imsave(exname,export(j))
        
def preproc_transfer(transfer,var=15):
    fn = Path(f'./Temp/translate_{var}.dat')
    #if fn.exists():
    #    with fn.open('rb') as file:
    #        return pickle.load(file)
    res = []
    for x in tqdm(range(np.max(transfer) + 2),desc='prebuild_transition'):
        filter_ = transfer > x
        w,h,c = filter_.shape
        assert c==1,"Cannot filter with more then one channel"
        filter2 = filters.gaussian(filter_.reshape(w,h),sigma=var).reshape(w,h,c)
        res.append(filter2)
    #with fn.open('wb') as file:
        
    #    pickle.dump(res,file)
    return res


    
    
    
    
@jit(parallel=True)
def fast_paint(prev_array1,current_array1,transfer_array1):
    w,h,c,_ = prev_array1.shape
    _,_,_,x = transfer_array1.shape
    prev_array2 = np.broadcast_to(prev_array1,(w,h,c,x))
    current_array2 = np.broadcast_to(current_array1,(w,h,c,x))
    transfer_array2 = np.broadcast_to(transfer_array1,(w,h,c,x))
    return prev_array2*transfer_array2 + current_array2*(1-transfer_array2)

def fast_paint_wrapper(prev_array,current_array,transfer_array):
    shader_array = np.stack(transfer_array).transpose(1,2,0,3)[:,:,None,:,0]
    prev_array = prev_array[:,:,:,None]
    current_array = current_array[:,:,:,None]
    print(prev_array.shape,current_array.shape,shader_array.shape)
    return fast_paint(prev_array,current_array,shader_array)
    
@jit(nopython=True,parallel=True)
def fast_mult(prev1,current1,filter1,keyframe):
    r = (prev1 * filter1) + (current1* (1-filter1))
    return r, np.sum(np.abs(r-keyframe)) > 100

    
def slow_paint(prev,current,transfer):
    #assert len(transfer.shape) == 2,f"transfer has shape {transfer.shape}"
    keyframe = prev
    yield keyframe
    for filter_ in transfer:
        filter_ = np.broadcast_to(filter_,prev.shape)
        res,t = fast_mult(prev,current,filter_,keyframe)
        if t:
            keyframe = res
            yield keyframe
        
    
    #return (prev * filter_) + (current* (1-filter_))

        
        
    
def red(mx=60,test=False):
    vvvv = iter(Path('./Output_Test').glob('*.mp4'))
    while True:
        try:
            r = next(vvvv)
            print(r.absolute())
            name = r.stem
            stop = False
        except StopIteration:
            r = choice(list(scanv()))
            print(r.absolute())
            name = r.stem
            stop = True
        x = Path('./Cleaned') / (name+'.png')
        
        target = (Path('./Output_Test' if test else './Output') / name).with_suffix('.mp4')
        
        if target.exists() and not test:
            continue
        
        image = io.imread(str(x))
        orig = color.rgb2lab(image)
        model = calculate_clustering(orig, 'r/' + name)
        w,h,c = orig.shape
        stack = {}
        
        for x in tqdm(range(1,mx+1),desc='reducing'):
            stack[x] = reduce_colors(orig,model,x)
            
        
        loop = {}
        
        for k in tqdm(stack,desc='linking'):
            if k+1 not in stack:
                continue
            loop[k] = {}
            old = stack[k][0]
            new = stack[k+1][0]
            newset = set(new)
            counter = Counter(list(new))
            for x in set(old):
                counter = Counter(list(new[old == x]))
                #print(counter)
                loop[k][x] = max(newset, key = lambda x:counter.get(x,0))
        
        #print(loop)
        
        for k in tqdm(sorted(loop,key=lambda x:-x),desc='colors'):
            labels, colors = stack[k]
            _,colors_prev = stack[k+1]
            #print(colors.shape,colors_prev.shape)
            #print(colors)
            for x in range(k):
                colors[x,:] = colors_prev[loop[k][x],:]
            #print(colors)
            stack[k] = labels, colors
            
        imgs = []
        
        for k,v in tqdm(stack.items(),desc='building images'):
            img = export(build_with_colors(v[0],v[1],(w,h,c)))
            imgs.append(img)
            exname = (Path('./Temp') / name / f'{k:04}').with_suffix('.png')
            exname.parent.mkdir(exist_ok=True,parents=True)
            io.imsave(exname,img)
            
        final = stack[max(stack)][0]
        imgs_final = imgs[-1]
        w,h,c = imgs_final.shape
        for k in tqdm(reversed(range(max(stack)+2)),desc='building final'):
            filter_ = (final < k).reshape(w,h,1)
            img = imgs_final*filter_ + image*~filter_
            imgs.append(img)
            exname = (Path('./Temp') / name / f'{k:04}_').with_suffix('.png')
            exname.parent.mkdir(exist_ok=True,parents=True)
            io.imsave(exname,img)
            
        print('build transition')
        transfer = build_stokes(w,h,30,10)[:,:,None]
        print('build transition')
            
        clip = Clip.build(chain(slow_paint_clip(transfer,imgs),[image]*24*30))
        
        clip.save(str(target))
        
        if test:
            break
            
def build_stokes(w,h,brushsize,speed):
    x = np.indices((w,)).reshape(-1,1)
    y = np.indices((h,)).reshape(1,-1)
    
    strokes = (x // brushsize) * (h//speed)
    return (y//speed + strokes) * ((x // brushsize)%2) + ((h-y)//speed + strokes) * (((x // brushsize)+1)%2)

def diter_transfer(transfer,var=15):
    print(transfer.shape)
    w,h = transfer.shape
    x = np.indices((w,)).reshape(-1,1)
    y = np.indices((h,)).reshape(1,-1)
    xshift = np.round(np.random.normal(0,var,transfer.shape)).astype(np.int)
    yshift = np.round(np.random.normal(0,var,transfer.shape)).astype(np.int)
    xloc = np.clip(xshift + x,0,w-1)
    yloc = np.clip(yshift + y,0,h-1)
    
    res = transfer.copy()
    for x in range(w):
        for y in range(h):
            res[x,y] = transfer[xloc[x,y],yloc[x,y]]
    return res
           
           
def fade(start,end,n=24*10):
    for x in np.linspace(0,1,n):
        yield end*x + start*(1-x)
    
@lru_cache(maxsize=128, typed=False)  
def get_conv_matr(sigma):
    gx = np.arange(-sigma*2, sigma*2)
    gaussianx = np.exp(-(gx/sigma)**2/2)
    gaussiany = gaussianx.reshape(1,-1)
    gaussian = gaussianx*gaussiany
    gaussian /= np.sum(gaussian)   
    return gaussian
            
def fade_convolve(orig,sigma):
    w,h = orig.shape
    
    result = scipy.signal.convolve(orig, get_conv_matr(sigma),mode='same')
    return  result
        
def slow_paint_clip(transfer,imgs):
    #print(transfer)
    transfer_proc = preproc_transfer(transfer)
    print(np.stack(transfer_proc).shape)
    #transfer_proc = np.stack(transfer_proc).transpose((1,2,0,3))[:,:,None,:,0]
    prev = None
    for s in tqdm(imgs,'applying transitions'):
        if prev is None:
            prev = s
            continue
        yield from slow_paint(prev,s,transfer_proc)
        prev = s
            

        
    

class Clip:
    def __init__(self):
        self.images = []
        self.clips = []
        self.fps = 24
        self.counter = 0

    def add(self, gen):
        for x in gen:
            self.images.append(x)
            if len(self.images) >= 200:
                self.flush()

    def flush(self):
        try:
            v = VideoClip(self.get, duration=len(self.images) / self.fps)
            v.write_videofile('Temp2/' + str(self.counter) + '.mp4', fps=24)
            vv = VideoFileClip('Temp2/' + str(self.counter) + '.mp4')
            self.counter += 1
            self.clips.append(vv)
            self.images[:] = []
        except IndexError:
            pass

    def get(self, t):
        try:
            return self.images[int(t * self.fps)]
        except IndexError:
            return self.images[-1]

    def clip(self):
        self.flush()
        return concatenate_videoclips(self.clips)
    
    def save(self,fname):
        self.clip().write_videofile(fname, fps=self.fps)

    @staticmethod
    def build(g):
        c = Clip()
        c.add(g)
        return c
            
            
if __name__ == "__main__":
    p = argparse.ArgumentParser(description=__doc__,
            formatter_class=argparse.RawDescriptionHelpFormatter)
        
    p.add_argument("sub",
                    help="sub program")  
        
    args = p.parse_args()
    if args.sub == "preproc":
        Path("./Cleaned").mkdir(exist_ok=True)
        Path("./Output").mkdir(exist_ok=True)
        Path("./Output_Test").mkdir(exist_ok=True)
        Path("./Temp").mkdir(exist_ok=True)
        Path("./Temp2").mkdir(exist_ok=True)
        preproc()  
    elif args.sub == "test":
        red(5,True)    
    elif args.sub == "run":
        red()
    else:
        raise AssertionError(f"Invalid argument {args.sub} for sub")
    

    
    
    
    
    