TIME=$(ffprobe -v error -show_entries format=duration -of default=noprint_wrappers=1:nokey=1 transition/full.mp4)
ffmpeg -loop 1 -i $1 -loop 1 -i $2 -i transition/full.mp4 -t $TIME \
       -filter_complex "[1][2]alphamerge[ovrly];[0][ovrly]overlay=0:0" $3