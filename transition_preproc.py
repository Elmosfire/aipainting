import numpy as np
from pathlib import Path
from skimage import data, io, filters, draw, transform, color
from tqdm import tqdm
from video import save_grey

def build_stokes(w,h,brushsize,speed):
    x = np.indices((w,)).reshape(-1,1)
    y = np.indices((h,)).reshape(1,-1)
    
    strokes = (x // brushsize) * (h//speed)
    return (y//speed + strokes) * ((x // brushsize)%2) + ((h-y)//speed + strokes) * (((x // brushsize)+1)%2)

def preproc_transfer(transfer,var=15):
    for x in tqdm(range(np.max(transfer) + 2),desc='prebuild_transition'):
        filter_ = transfer > x
        w,h,c = filter_.shape
        assert c==1,"Cannot filter with more then one channel"
        filter2 = filters.gaussian(filter_.reshape(w,h),sigma=var).reshape(w,h)
        yield filter2
        
        

root = Path('./transition')
root.mkdir(exist_ok=True)

def get_frames():
    for i,transfer in enumerate(preproc_transfer(build_stokes(480,640,30,20)[:,:,None])):
        np.save((root / f"{i:04}").with_suffix(".npy"),transfer)
        
        yield transfer


save_grey(get_frames(),str(root / "full.mp4"))