import threading, signal, os, sys, time, logging
import numpy as np

import subprocess as sp
from moviepy.editor import VideoClip, concatenate_videoclips, VideoFileClip
from random import choice
from pathlib import Path
import time
from random import shuffle

frame_width= 640
frame_height=480
FPS = 24



def scanv():
    yield from Path('./storage').glob('*/final.mp4')

command1 = [
    'sox',
    'bg.mp3',
    'bgl.mp3',
    'repeat', str(30*24)
]


CBR='4500k'
command = [
    'ffmpeg',
    '-f', 'rawvideo',
    '-vcodec','rawvideo',
    '-s', str(frame_width)+'x'+str(frame_height),
    '-pix_fmt', 'rgb24',
    '-re', 
    '-i', '-',
    '-stream_loop', '-1',
    '-i', 'bgl.mp3',
    '-f', 'flv',
    '-vcodec', 'libx264',
    '-profile:v', 'main',
    '-g', '60',
    '-keyint_min', '30',
    '-b:v', CBR,
    '-minrate', CBR,
    '-maxrate', CBR,
    '-pix_fmt', 'yuv420p',
    '-preset', 'ultrafast',
    '-tune', 'zerolatency',
    '-bufsize', CBR,
    '-t',str(60*60*24),
    'rtmp://ams02.contribute.live-video.net/app/live_153039198_gAgkFlaswoFY1Qz8IwbMTS5ImEZNlR',
    
    #'test.mp4'
    ]

if True:
    sp.Popen(command1, stdin=sp.PIPE, stderr=sp.STDOUT, bufsize=0)
proc = sp.Popen(command, stdin=sp.PIPE, stderr=sp.STDOUT, bufsize=0)

try:
    
    while True:
        l = list(scanv())
        shuffle(l)
        assert l, "No valid videos"
        for fname in l:
            print(fname)
            v = VideoFileClip(str(fname))
            stp = time.time() + 1/24
            for frame in v.iter_frames():
                proc.stdin.write(frame.tostring())
                #time.sleep(stp-time.time())

finally:
    proc.stdin.close()
    proc.wait()