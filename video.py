from moviepy.editor import VideoClip, concatenate_videoclips, VideoFileClip
import numpy as np
from tempfile import TemporaryFile  
from skimage import data, io, filters, draw, transform, color
from pathlib import Path

Path('./tmp').mkdir(exist_ok=True)

def export(img):
    return (255 * color.lab2rgb(img)).astype('uint8')

def export_grey(img):
    w,h = img.shape
    img = np.broadcast_to(img[:,:,None],(w,h,3))
    return (255 * img).astype('uint8')

class Clip:
    def __init__(self):
        self.images = []
        self.fps = 24
        self.counter = 0

    def add(self, gen):
        for x in gen:
            print('shape:',x.shape)
            self.images.append(export(x))
            
    def add_grey(self, gen):
        for x in gen:
            self.images.append(export_grey(x))

    def clip(self):
        return VideoClip(self.get, duration=len(self.images) / self.fps)


    def get(self, t):
        try:
            return self.images[int(t * self.fps)]
        except IndexError:
            return self.images[-1]

    def save(self,fname):
        self.clip().write_videofile(fname, fps=self.fps)


def save(gen_,fname):
    c = Clip()
    c.add(gen_)
    return c.save(fname)

def save_grey(gen_,fname):
    c = Clip()
    c.add_grey(gen_)
    return c.save(fname)

def concat(gen_,fname):
    concatenate_videoclips([VideoFileClip(str(x)) for x in gen_]).write_videofile(fname)
        


    